'use strict';
var UserModel = require('../models/user');
var _ = require('lodash');

module.exports.sessionUser = function (req, res, next) {
  UserModel
    .findOne(req.user._id)
    .then(function (user) {
      if (user) {
        return res.status(200).json({status: 'Authorized', user: user}); //TODO: what return?
      }
      var err = new Error('Not found user');
      err.status = 404;
      return next(err);
    }).catch(next);
};