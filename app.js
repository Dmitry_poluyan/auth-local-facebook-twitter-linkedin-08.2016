'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');
var passport = require('passport');
var MongoStore = require('connect-mongo')(session);

var config = require('./config');
var log = require('./libs/log')(module);
var dbConnect = require('./libs/mongoose');
require('./libs/passport')(passport);

var users = require('./routes/user');
var auth = require('./routes/auth');

var app = express();

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(session({
  secret: config.get('session:secret'),
  key: config.get('session:key'),
  cookie: config.get('session:cookie'),
  store: new MongoStore({mongooseConnection: dbConnect}),
  resave: config.get('session:resave'),
  saveUninitialized: config.get('session:saveUninitialized')
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/api/user', users);
app.use('/api/auth', auth);

// error handlers
app.use(function (error, req, res, next) {
  var status = error.status ? error.status : 500;
  log.error('%s %d %s', req.method, status, error.message);

  return res.status(status).json(error);
});

app.listen(config.get('port'), function () {
  log.info('Sever run ' + config.get('port'));
});

module.exports = app;
