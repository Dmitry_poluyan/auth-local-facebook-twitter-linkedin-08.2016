'use strict';
var UsersCtrl = require('../controllers/user');
var checkAuth = require('../libs/checkAuth');
var log = require('../libs/log')(module);

var express = require('express');
var router = express.Router();

router.use(function timeLog(req, res, next) {
  log.info('Time: ', Date.now());
  next();
});

router.use(checkAuth);

router.get('/me', checkAuth, UsersCtrl.sessionUser);

module.exports = router;