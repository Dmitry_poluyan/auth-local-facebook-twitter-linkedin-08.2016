'use strict';
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');

var Schema = mongoose.Schema;

var User = new Schema({ //todo: How need schema?
  local: {
    username: String,
    password: String
  },
  facebook: {
    id: String,
    token: String,
    email: String,
    username: String
  },
  twitter: {
    id: String,
    token: String,
    displayName: String,
    username: String
  },
  linkedin: {
    id: String,
    token: String,
    displayName: String,
    username: String
  },
  provider: String
});

User.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
};

User.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', User);